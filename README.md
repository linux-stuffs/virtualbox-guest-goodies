# VirtualBox Guest Goodies

A collections of goodies for the unix-like VirtualBox quests.

## VBoxSharedFolders
VBoxSharedFolders is simply script for mount / unmount Virtualbox shared
folders for Linux based distributions and was wroted like the hack due the
Virtualbox problems with the auto mounting of these shared folders

Usage:
```
  VBoxSharedFolders [--mount | --mnt | -m | --umount | --umt | -u]
```

Options:

* `--mount, --mnt, -m` - mount VirtualBox shared folders to /media
* `--umout, --umt, -u` - unmount all these VirtualBox shared folders



## VBoxXrandr

VBoxXrandr is simply script for set a custom screen resolution which was
wroted like the hack due the Virtualbox problems with the resizing in the
VMSVGA display mode

Usage:
```
   VBoxXrandr [--auto | -a ] x y refresh
   VBoxXrandr [--manually | -m ] screen_name x y refresh
```

Options:
* `-a, --auto` - set screen resolution. The screen name is autodetected.
* `-m, --manually` - set screen resolution. The screen name must to be like a parameter.

### Example settings for the lightdm
(/etc/lightdm/lightdm.conf)
```
. . .
[Seat:*]
display-setup-script=/usr/bin/VBoxXrandr -a 1920 975 60.00
. . .
```

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/virtualbox-guest-goodies/):
```
git clone https://aur.archlinux.org/virtualbox-guest-goodies.git
cd virtualbox-guest-goodies/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest bash-conf package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

then run this command (like root):
```
apt install ./virtualbox-guest-goodies*.deb
```

### Install from source:
Unpack the source package and run command (like root):
```
./configure
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
./configure
make build-deb
```

### Install from *.deb package:
```
apt install ./distrib/virtualbox-guest-goodies*.deb
```

### Build ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
./configure
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U virtualbox-guest-goodies*.pkg.tar.xz
```
